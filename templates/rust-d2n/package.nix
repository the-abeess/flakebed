{ config, dream2nix, ... }:
let
  cargoToml =
    builtins.fromTOML
    (builtins.readFile "${config.paths.projectRoot}/Cargo.toml");
in {
  imports = [
    dream2nix.modules.dream2nix.rust-cargo-lock
    dream2nix.modules.dream2nix.rust-crane
  ];

  deps = {nixpkgs, ...}: {
    inherit (nixpkgs) fetchFromGitHub;
  };

  inherit (cargoToml.package) name version;

  mkDerivation = {
    src = config.paths.projectRoot;
  };

  rust-crane = {
    buildProfile = "release";
    buildFlags = ["--verbose"];
    runTests = true;
  };
}
