{
  inputs = {
    abeess = {
      url = "gitlab:abeess/flakebed";
      inputs.parts.follows = "dream2nix/flake-parts";
    };
    dream2nix.url = "github:nix-community/dream2nix";
  };

  outputs = inputs:
    inputs.abeess.lib.mkFlake
    { inherit inputs; }
    ({ config, inputs, libs, ... }:
    let
      pkgModule = ./package.nix;
    in {
      systems = libs.systems.flakeExposed;
      nixpkgs.source = inputs.dream2nix.inputs.nixpkgs;

      perSystem = { pkgs, ... }: {
        packages."my-crate" = inputs.dream2nix.lib.evalModules {
          packageSets.nixpkgs = pkgs;
          modules = [
            inputs.dream2nix.modules.dream2nix.rust-cargo-lock
            inputs.dream2nix.modules.dream2nix.rust-crane
            {
              paths.projectRoot = ./.;
              paths.projectRootFile = "flake.nix";
              paths.package = pkgModule;
            }
            pkgModule
          ];
        };
      };
    });
}
