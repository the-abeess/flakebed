//! TODO

#[cfg(all(feature = "bevy-stable", not(feature = "bevy-latest")))]
pub(crate) use bevy_stable as bevy;
#[cfg(feature = "bevy-latest")]
pub(crate) use bevy_latest as bevy;
#[cfg(all(feature = "bevy-stable", feature = "bevy-latest"))]
pub(crate) use bevy_stable;
