{
  inputs = {
    nci.url = "github:yusdacra/nix-cargo-integration";
    abeess = {
      url = "gitlab:the-abeess/libnix";
      inputs.parts.follows = "nci/parts";
    };
    devshell = {
      url = "github:numtide/devshell";
      inputs = { nixpkgs.follows = "nci/nixpkgs"; };
    };
  };

  outputs = inputs:
    inputs.abeess.lib.mkFlake
    { inherit inputs; }
    ({ libs, ... }: {
      systems = libs.systems.flakeExposed;
      imports = [ inputs.devshell.flakeModule inputs.nci.flakeModule ];

      nixpkgs.source = inputs.nci.inputs.nixpkgs;
      
      perSystem = { lib, pkgs, config, system, ... }: let

        inherit (lib) hiPrio;
      
        mkEnvSuffix = name: suffix: "\${${name}:+\$${name}:}${suffix}";

      in {
        nci.projects.my-crate = {
          path = ./.;
          export = false;
        };

        nci.crates.my-crate  = {
          export = true;
        };

        devshells.default = {
          devshell.name = "my-crate";
          devshell.packages = [
            pkgs.cargo-audit
            pkgs.cargo-bloat
            pkgs.cargo-deny
            pkgs.cargo-expand
            pkgs.cargo-feature
            pkgs.cargo-flamegraph
            pkgs.cargo-generate
            pkgs.cargo-llvm-cov
            pkgs.cargo-watch
            pkgs.clang
            pkgs.just
            pkgs.libllvm
            pkgs.lldb
            pkgs.pkg-config
          ];
          commands = [
            { package = pkgs.nixpkgs-fmt; category = "nix"; }
            {
              package = hiPrio config.nci.toolchains.shell;
              name = "cargo";
              help = pkgs.cargo.meta.description;
              category = "rust";
            }
          ];
          env = [
            { name = "LIBRARY_PATH"; eval = "$DEVSHELL_DIR/lib"; }
            {
              name = "LD_LIBRARY_PATH";
              eval = mkEnvSuffix "LD_LIBRARY_PATH" "$DEVSHELL_DIR/lib";
            }
            {
              name = "C_INCLUDE_PATH";
              eval = mkEnvSuffix "C_INCLUDE_PATH" "$DEVSHELL_DIR/include";
            }
            {
              name = "PKG_CONFIG_PATH";
              eval = mkEnvSuffix "PKG_CONFIG_PATH" "$DEVSHELL_DIR/lib/pkgconfig";
            }
            { name = "CFLAGS"; eval = ''"-I $DEVSHELL_DIR/include"''; }
            {
              name = "RUST_SRC_PATH";
              eval = "$DEVSHELL_DIR/lib/rustlib/src/rust/library";
            }
            { name = "RUST_LOG"; value = "warn"; }
          ];
        };

      };
    });
}
