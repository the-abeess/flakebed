local:
{ config, flake-parts-lib,  ... }:
let
  nixpkgs-lib = local.inputs.parts.inputs.nixpkgs-lib.lib;

  inherit (nixpkgs-lib) foldlAttrs genAttrs mkOption recursiveUpdate types;

  mergeLibs = libs:
    foldlAttrs
      (acc: name: value:
        builtins.addErrorContext
        "while merging attribute \"${name}\" under option `_module.args.libs`"
        (
          if acc ? ${name} && value ? ${name} then mkOption acc name
          else recursiveUpdate acc value
        )
      )
      { }
      libs
    // libs;

  mkConflict = acc: name:
    acc // {
      "${name}" =
        throw "attribute ${name} had conflicting values and couldn't be merged";
    };

in
{
  options = {
    libs = mkOption {
      type = types.submodule {
        freeformType = types.lazyAttrsOf types.raw;
      };
      default = {};
    };
    flake.lib = mkOption {
      type = types.submodule {
        freeformType = types.lazyAttrsOf types.raw;
        options =
          genAttrs
          [ "types" ]
          (_: mkOption {
            type = types.submodule {
              freeformType = types.lazyAttrsOf types.raw;
            };
            default = {};
          });
      };
      default = {};
    };
  };

  config = {
    libs = {
      nixpkgs = nixpkgs-lib;
      parts = flake-parts-lib;
      abeess = local.config.flake.lib;
    };
    _module.args.libs = mergeLibs config.libs;
  };
}
