_:
{ libs, config, extendModules, ... }:
let
  inherit (libs)
    attrNames concatMap head mapAttrsToList mkEnableOption mkForce mkIf
    zipAttrsWith;

  topCfg = config;
in
{
  options.crossPackages = {
    enable = mkEnableOption "automic definition of cross-system packages";
  };

  config.perSystem = { systemInfo, config, ... }:
  {
    packages =
      let
        crossCfg = extendModules {
          modules = [{
            perSystem.nixpkgs.buildPlatform = systemInfo;
            crossPackages.enable = mkForce false;
          }];
        };
        pkgs = crossCfg.config.flake.packages or {};
      in mkIf topCfg.crossPackages.enable (
        zipAttrsWith
        (_: p: head p)
        (
          concatMap
          (sys:
            if sys == systemInfo.system then [ ]
            else
              mapAttrsToList
              (name: drv: { "${name}-${sys}" = drv; })
              pkgs.${sys}
          )
          (attrNames pkgs)
        )
      );
  };
}
