local:
{ config, inputs, libs, ... }:
let

  inherit (libs)
    literalExpression literalMD mdDoc mkOption mkOptionDefault mkOverride
    recursiveUpdate types;

  noNixpkgsError = ''
    abeess.flakeModules.nixpkgs: The flake does not have a `nixpkgs` input.
    To solve this error, you may:
      - Add the input to your flake
      - Set the `nixpkgs.source` option
  '';

  mkAlmostOptionDefault = mkOverride ((mkOptionDefault { }).priority - 100);

  topCfg = config.nixpkgs;

  nixpkgsOptions = {
    source = mkOption {
      type = types.flake;
      description = mdDoc ''
        A path to the nixpkgs source.
      '';
      default =
        builtins.seq
        (inputs.nixpkgs or (throw noNixpkgsError))
        inputs.nixpkgs;
      defaultText = literalMD "`inputs.nixpkgs`";
    };

    overlays = mkOption {
      type = types.listOf (types.functionTo (types.functionTo types.attrs));
      description = mdDoc ''
        List of overlays to use with the Nix Packages collection.
        (For details, see the Nixpkgs documentation.)  It allows
        you to override packages globally. Each function in the list
        takes as an argument the *original* Nixpkgs.
        The first argument should be used for finding dependencies, and
        the second should be used for overriding recipes.

        If `nixpkgs.pkgs` is set in a per-system config, overlays specified here
        will be applied after the overlays that were already present
        in `nixpkgs.pkgs`, but before overlays specifed in the same per-system
        config.
      '';
      default = [ ];
      example = literalExpression
        ''
          [
            (self: super: {
              openssh = super.openssh.override {
                hpnSupport = true;
                kerberos = self.libkrb5;
              };
            })
          ]
        '';
    };

    config = mkOption {
      type = types.attrs;
      description = mdDoc ''
        The configuration of the Nix Packages collection.  (For
        details, see the Nixpkgs documentation.)  It allows you to set
        package configuration options.

        Ignored when `nixpkgs.pkgs` is set.
      '';
      default = { };
      example = literalExpression
        ''
          { allowBroken = true; allowUnfree = true; }
        '';
    };
  };
in
{
  options.nixpkgs = nixpkgsOptions;

  config = {
    perSystem = { config, options, system, ... }:
      let
        cfg = config.nixpkgs;
        opt = options.nixpkgs;

        nixpkgsOptionUpdates = {
          source.default = topCfg.source;
          source.defaultText =
            literalMD
              "top-level `nixpkgs.source` (if set), or top-level `inputs.nixpkgs`";

          overlays.apply = l: l ++ topCfg.overlays;

          config.apply = s: recursiveUpdate topCfg.config s;

          buildPlatform = mkOption {
            type = types.either types.str types.attrs;
            default = system;
            example = {
              system = "aarch64-linux";
              config = "aarch64-unknown-linux-gnu";
            };
            apply = libs.systems.elaborate;
          };

          pkgs = mkOption {
            type = types.packageSet;
            description = ''
              If set, the pkgs argument to all flake modules is the value of
              this option, extended with both top-level and per-system
              `nixpkgs.overlays`, if also set. Other options in `nixpkgs.*`,
              notably `config`, will be ignored (both in top-level and per-system
              configs).

              If unset, the pkgs argument to all flake modules is determined
              as shown in the default value for this option.

              The default value imports the Nixpkgs source files from
              `nixpkgs.source` and applies this option's `config`, and `overlays`
              siblings.

              See `nixpkgs.buildPlatform` for how the `localSystem` and
              `crossSystem` arguments are set.
            '';
            default = import topCfg.source finalNixpkgsConfig;
            defaultText = literalExpression ''
              import config.nixpkgs.source {
                inherit (config.nixpkgs) config overlays;
              }
            '';
          };
        };

        finalNixpkgsConfig =
          let
            isCross = cfg.buildPlatform.system != system;
          in
          {
            inherit (cfg) config;
            localSystem = if isCross then system else cfg.buildPlatform;
            ${if isCross then "crossSystem" else null} = cfg.buildPlatform;
          };
      in
      {
        options.nixpkgs = recursiveUpdate nixpkgsOptions nixpkgsOptionUpdates;

        config = {
          _module.args.pkgs =
            mkAlmostOptionDefault
              (
                if opt.pkgs.isDefined
                then cfg.pkgs.appendOverlays cfg.overlays
                else cfg.pkgs
              );
        };
      };
  };
}
