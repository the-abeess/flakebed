local:
{ libs, ... }:
{
  config.perSystem = { system, ... }: {
    _module.args.systemInfo = libs.systems.elaborate system;
  };
}
