{ libs, ...}:
let
  inherit (libs.nixpkgs)
    addErrorContext attrNames concatMap elem elemAt filter hasPrefix hasSuffix
    isAttrs isList length lists listToAttrs mkOption;
  inherit (libs.nixpkgs.types) optionDescriptionPhrase;

  types = libs.nixpkgs.types // libs.abeess.types;
in
{
  imports = [
    ./attrset.nix
    ./eval.nix
    ./flake.nix
    ./helix
    ./maybe.nix
    ./monitor.nix
    ./package.nix
    ./result.nix
    ./transphobeland.nix
    ./wireguard.nix
  ];

  flake.lib = {
    composeLibs = base: extension: base.extend (_: _: extension);

    importTree = let
      f = path: op: value:
        let
          subDirs =
            if ! isAttrs value then []
            else
              filter
              (subTree: ! elem subTree.value or null [ null {} [] ])
              (
                concatMap
                (dir: f "${path}/${dir}" op value.${dir})
                (filter (dir: ! hasPrefix "." dir) (attrNames value))
              );
          files = 
            map
            (file:
              let
                filename =
                  if hasSuffix ".nix" file then file
                  else "${file}.nix";
              in
              op "${path}/${filename}"
            )
            (if isList value then value else value."." or []);
        in
        # trace "---> ${path}"
        # trace value
        # trace files
        # trace subDirs
        # trace "<--- ${path}"
        addErrorContext "While import tree under ${path}" (files ++ subDirs);
    in
    path: op: values: listToAttrs (f path op values);

    types = {
      noDupListOf = elemType:
      let
        list = types.addCheck
          (types.listOf elemType)
          (l: length (lists.unique l) == length l);
      in
      list // {
        description =
          let
            inner = optionDescriptionPhrase (class: class == "noun") list;
          in
          "${inner} without dupplicates";
      };

    tupleOf = n: elemType:
      assert n > 0;
      let
        list = types.addCheck (types.listOf elemType) (l: length l == n);
        desc =
          optionDescriptionPhrase
            (class: class == "noun" || class == "composite")
            elemType;
      in
      list // {
        description = "${toString n}-tuple of ${desc}";
      };

    point2dOf = elemType:
      types.coercedTo
        (types.tupleOf 2 elemType)
        (tuple: { x = elemAt tuple 0; y = elemAt tuple 1; })
        (types.submodule {
          options = {
            x = mkOption { type = elemType; };
            y = mkOption { type = elemType; };
          };
        });
    };
  };
}

