{libs, ...}:
let
  inherit (libs.nixpkgs)
    any attrNames attrValues concatMap concatStringsSep elem elemAt filter
    filterAttrs head isAttrs isType length listToAttrs mapAttrs substring
    zipAttrsWith;
  inherit (libs.abeess)
    fallible flattenAttrsWith recursiveMapAttrsToList recursiveUpdateUnless
    result;
in
{
  flake.lib = {
    stripSpecialAttrs = filterAttrs (n: _: substring 0 1 n != "_");

    recursiveUpdateUnless =
      # Predicate: any -> any -> Fallible; any failure will be returned
      check:
      # Left attribute set of the merge.
      lhs:
      # Right attribute set of the merge.
      rhs:
      let
        f = attrPath: sets:
          let
            zipped =
              zipAttrsWith
                (n: values:
                  let
                    here = attrPath ++ [ n ];
                  in
                  if length values == 1 then result.some (head values)
                  else
                    result.andThen
                      (result.fromFallible
                        (fallible.map
                          (error: {
                            inherit error;
                            path = here;
                            __toString = err:
                              "Cannot recursively merge at "
                              + (concatStringsSep "." err.path)
                              + ": ${toString err.error}";
                          })
                          (check (elemAt values 1) (head values))
                        )
                      )
                      (_: f here values)
                )
                sets;
            failures = filter result.isFail (attrValues zipped);
          in
          if failures != [ ] then (head failures)
          else result.some (mapAttrs (_: result.unwrap) zipped);
      in
      f [ ] [ rhs lhs ];

    recursiveUpdateChecked = lhs: rhs:
      recursiveUpdateUnless
        (lhs: rhs:
          let
            msg = "conflict between value";
          in
          if ! (isAttrs lhs && isAttrs rhs) then
            result.fail "${msg}: not an attribute set"
          else if lhs ? outPath || rhs ? outPath then
            result.fail "${msg}: cannot merge sets representing store paths"
          else
          let
            canMergeNested = optType:
              let
                nestedTypes = attrValues optType.nestedTypes or {};
                mergeableTypes = [ "attrsOf" "lazyAttrsOf" "submodule" ];
              in
              if elem optType.name or "" mergeableTypes then fallible.none
              else if any (t: elem t.name mergeableTypes) nestedTypes then
                fallible.none
              else result.fail "Cannot merge values of option-type ${optType.name}";
            unmergeableOptions =
              filter
              (set: isType "option" set && result.isFail (canMergeNested set.type))
              [ lhs rhs];
          in
          if unmergeableOptions !=  [] then
            result.fail "${msg}: ${result.unwrapFail (head unmergeableOptions)}"
          else fallible.none
        )
        lhs
        rhs;

    recursiveMapAttrsToList = op: set:
      let
        f = path: value:
          if isAttrs value then
            concatMap
            (n: f (path ++ [n]) value.${n})
            (attrNames value)
          else [ (op path value) ];
      in f [] set;

    flattenAttrsWith = op: set:
      listToAttrs
      (recursiveMapAttrsToList (p: value: { name = op p; inherit value; }) set);

    flattenAttrs = flattenAttrsWith (path: concatStringsSep "-" path);
  };
}
