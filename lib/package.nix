{libs, ...}:
let
in
{
  flake.lib = {
    types.packageSet = 
      libs.nixpkgs.types.attrs;
  };
}
