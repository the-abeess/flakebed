{libs, config, ... }:
let
  inherit (libs) defaultFunctor mergeOneOption mkOptionType;
in
{
  flake.lib = {
    mkFlake = args: mod: libs.parts.mkFlake args {
      imports = [
        config.flake.flakeModules.default
        mod
      ];
    };
    
    types.flake = mkOptionType {
      name = "flake";
      description = "nix flake";
      descriptionClass = "noun";
      check = x: x._type or null == "flake";
      merge = mergeOneOption;
      functor = defaultFunctor "flake" // {
        type = libs.types.flake;
      };
    };
  };
}
