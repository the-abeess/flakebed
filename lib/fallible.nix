{libs, ...}:
let
  inherit (libs.nixpkgs)
    count defaultFunctor getFiles head isAttrs length mkOptionType showFiles
    showOption;
  inherit (libs.nixpkgs.types) optionDescriptionPhrase;
  inherit (libs.abeess) fallible;
in
{
  flake.lib = {
    fallible = {
      fail = fail: { inherit fail; };
      none = { };

      isNone = fl: ! fallible.isFail fl;
      isFail = fl: fl ? fail;

      map = op: f:
        if fallible.isNone f then f
        else fallible.fail (op (fallible.unwrap f));

      unwrap = f: f.fail or (throw "fallible: expected a failure");
    };
    
    # A type which is an attribut set that may represent failure.
    types.fallible = wrapped:
      let
        name = "fallible";
      in
      mkOptionType {
        inherit name;
        description =
          "fallible ${optionDescriptionPhrase (c: c == "noun") wrapped}";
        descriptionClass = "composite";
        check = x:
          isAttrs x
          && (x ? fail -> wrapped.check x.fail);
        merge = loc: defs:
          let nrNone = count (def: fallible.isNone def.value) defs;
          in if defs == [ ] then throw "unreachable"
          else if nrNone == length defs then (head defs).value
          else if nrNone == 0 then wrapped.merge loc defs
          else
            throw (
              "The option `${showOption loc}` is defined both as none and fail, in "
                "${showFiles (getFiles defs)}"
            );
        emptyValue = { value = { }; };
        inherit (wrapped) getSubOptions getSubModules;
        substSubModules = m: libs.abeess.types.fallible (wrapped.subsSubModules m);
        functor = (defaultFunctor name) // { inherit wrapped; };
        nestedTypes = { inherit wrapped; };
      };
  };
}
