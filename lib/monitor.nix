{libs, ...}:
let
  inherit (libs)
    any attrValues concatStringsSep elemAt isAttrs isList mapAttrs match
    mdDoc mkOption mkOptionDefault types;
  inherit (libs.strings) fromJSON;
  inherit (libs.monitor) spec;
in
{
  flake.lib.monitor = {
    resolution.toStr = res:
    if isList res then concatStringsSep "x" res
    else if isAttrs res then "${res.x}x${res.y}"
    else toString res;

    spec.pattern = "([[:digit:]]+)x([[:digit:]]+)(@([[:digit:]]+))?";

    spec.fromStr = spec':
      let
        groups = match spec.pattern spec';
        res = {
          x = elemAt groups 0;
          y = elemAt groups 1;
        };
        rate = elemAt groups 3;
      in
      if spec' == "preferred" then
        { resolution = "auto"; refreshRate = "auto"; }
      else if spec' == "highres" then
        { resolution = "highest"; refreshRate = "auto"; }
      else if spec' == "highrr" then
        { resolution = "auto"; refreshRate = "highest"; }
      else if isNull groups then
        throw ''
          Invalid monitor spec string: "${spec'}".
          Expected "preferred", "highres", "highrr", or a string like "1920x1080" or "1920x1080@60"
        ''
      else {
        ${if any isNull (attrValues res) then null else "resolution"} =
          mapAttrs (_: fromJSON) res;
        ${if rate == null then null else "refreshRate"} = fromJSON rate;
      };

    spec.toStr = { resolution, refreshRate }:
      if resolution == "auto" && refreshRate == "auto" then "preferred"
      else if resolution == "highest" then
        if refreshRate == "auto" then "highres"
        else
          throw ''
            Invalid monitor spec: refreshRate must be `"auto"` if resolution is `"highest"`.
            But refreshRate = ${toString refreshRate}
          ''
      else if refreshRate == "highest" then
        if resolution == "auto" then "highrr"
        else
          throw ''
            Invalid monitor spec: resolution must be `"auto"` if refreshRate is `"highest"`.
            But refreshRate = ${libs.abeess-config.resolution.toStr resolution}
          ''
      else
        let
          resolution = libs.abeess-config.resolution.toStr resolution;
        in
        if refreshRate == "auto" then resolution
        else "${resolution}@${toString refreshRate}";
  };

  flake.lib.types.monitor = {
    resolution =
      types.either
        (types.point2dOf types.ints.unsigned)
        (types.enum [ "auto" "highest" ])
      // {
        description =
          "width (x-dimension) and height (y-dimension) of a monitor, in pixels";
      };

    refreshRate = types.either types.ints.unsigned (types.enum [ "auto" "highest" ]) // {
      description = "refresh rate of a monitor, in frames per second";
    };

    specSet =
      types.addCheck
        (types.submodule {
          options = {
            resolution = mkOption {
              type = types.monitor.resolution;
              default = "auto";
              example = [ 1920 1080 ];
              description = mdDoc ''
                Width (x-dimension) and height (y-dimension) of a monitor in pixels.
              '';
            };

            refreshRate = mkOption {
              type = types.monitor.refreshRate;
              default = "auto";
              example = 60;
              description = mdDoc ''
                Refresh rate of a monitor, in frames per second.
              '';
            };
          };
        })
        ({ resolution, refreshRate }:
          if refreshRate == "highest" then resolution == "auto"
          else if resolution == "highest" then refreshRate == "auto"
          else if resolution == "auto" then refreshRate == "auto"
          else true
        )
      // {
        description =
          "width (x-dimension), height (y-dimension) and refresh rate of a "
          + "monitor";
      };

    specStr = types.oneOf [
      (types.strMatching spec.pattern)
      (types.enum [ "preferred" "highres" "highrr" ])
    ]
    // {
      description = "resolution & refresh rate of a monitor";
    };

    spec =
      types.coercedTo
        types.monitor.specStr
        spec.fromStr
        types.monitor.specSet;

    offset = types.oneOf [
      (types.point2dOf types.ints.unsigned)
      (types.enum [ "auto" ])
    ]
    // {
      description = "position offset (in pixels) of a monitor";
    };

    display = types.submodule ({ name, ... }: {
      options = {
        name = mkOption {
          type = types.str;
          example = "DP-1";
          description = mdDoc ''
            The name of the monitor.

            `"default"` specifies any unconfigured monitor, like dynammically
            plugged-in monitors.
          '';
        };

        spec = mkOption {
          type = types.monitor.spec;
          default = "preferred";
          example = {
            resolution = { x = 1920; y = 1080; };
            refreshRate = 60;
          };
          description = mdDoc ''
            The monitor's spec, indicating a resolution and refresh rate.

            The following are valid specs:
            - An attribute set specifying a `resolution` and a `refreshRate`
              attribute.
            - A string like `"1920x1080@60"`.
            - `"preferred"`, to automatically choose resolution and rate.
            - `"highres"`, to choose the monitor's highest available resolution,
              and automatically choose among refresh rates available at this
              setting.
            - `"highrr"`, to choose the monitor's highest available refresh rate,
              and automatically choose among resolutions available at this
              setting.

            The following are valid resolutions (unit is in pixels):
            - An attribute set specifying an `x` and a `y` integer attribute.
            - A list of 2 integers, where the first is width and second height.
            - `"auto"`, to automatically choose the resolution (may depend on the
              chosent refresh rate).
            - `"highest"`, to automatically choose the highest resolution
              available.

            The following are valid refresh rates (unit is in Hertz):
            - An integer.
            - `"auto"`, to automatically choose the rate (may depend on the
              chosent resolution).
            - `"highest"`, to automatically choose the highest refresh rate
              available.

            Wherever resolution and refresh rate are both specified, leaving the
            refresh rate unspecified is equivalent to setting it to `"auto"`; and
            resolution may be left unspecified (or set to `"auto"`) if the
            refresh is `"highest"`.
          '';
        };

        offset = mkOption {
          type = types.monitor.offset;
          default = "auto";
          example = { x = -1920; y = 1080; };
          description = mdDoc ''
            The position of the monitor.

            To move a monitor left (right), decrease (increase) its offset along
            `x`. To move a monitor up (down), decrease (increase) its offset
            along `y`.

            The offset is applied before the transform, if any.
          '';
        };

        scale = mkOption {
          type = types.number;
          default = 1;
          example = 2;
          description = mdDoc ''
            The scale of the monitor.

            The scale is applied before the offset.
          '';
        };

        bitdepth = mkOption {
          type = types.enum [ "auto" 6 8 10 12 ];
          default = "auto";
          example = 10;
          description = mdDoc ''
            The bit depth used by the monitor.

            Not all environments may support all values. Leaving it to `"auto"`
            or unspecified should choose an appropriate default.
          '';
        };
      };

      config.name = mkOptionDefault name;
    });
  };
}
