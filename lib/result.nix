{libs, ...}:
let
  inherit (libs.nixpkgs)
    count defaultFunctor elemAt getFiles length mkOptionType showFiles
    showOption;
  inherit (libs.nixpkgs.types) optionDescriptionPhrase;
  inherit (libs.abeess) fallible result;
in
{
  flake.lib = {
    result = {
      some = some: { inherit some; };
      fail = fail: { inherit fail; };

      isSome = r: r ? some;
      isFail = r: r ? fail;

      unwrap = r:
        r.some or (
          builtins.addErrorContext "while unwrapping some result"
            (throw (toString (result.unwrapFail r)))
        );
      unwrapOr = dft: r: r.some or dft;
      unwrapFail = r:
        r.fail or (
          builtins.addErrorContext "while unwrapping a result's failure"
            (throw (
              if result.isSome r then "result is successful"
              else
                builtins.trace "invalid result:"
                builtins.trace r "result is invalid"
            ))
        );

      andThen = r: op: if result.isFail r then r else op r.some;

      fromFallible = f: if fallible.isFail f then f else result.some null;
    };

    # A type which is an attribute set that either hold some value, or represent
    # failure.
    types.result = some: fail:
      let
        name = "result";
        functor = (defaultFunctor name) // { wrapped = [ some fail ]; };
      in
      mkOptionType {
        inherit name;
        description =
          (optionDescriptionPhrase
            (class: class == "noun" || class == "conjunction")
            some
          )
          + " (in case of success), or "
          + (optionDescriptionPhrase
            (class: class == "noun" || class == "conjunction" || class == "composite")
            fail
          )
          + "(in case of error)";
        descriptionClass = "composite";
        check = x:
          (! result.isSome x -> (result.isFail x && fail.check x.fail))
          && (! result.isFail x -> (result.isSome x && some.check x.some));
        merge = loc: defs:
          let nrFail = count (def: result.isFail def.value) defs;
          in if defs == [ ] then throw "unreachable"
          else if nrFail == length defs then
            fail.merge loc (map result.unwrapFail defs)
          else if nrFail == 0 then some.merge loc (map result.unwrap defs)
          else
            throw (
              "The option `${showOption loc}` is defined both as some and fail, in "
                "${showFiles (getFiles defs)}"
            );
        typeMerge = f':
          let
            msome = some.typeMerge (elemAt f'.wrapped 0).functor;
            mfail = fail.typeMerge (elemAt f'.wrapped 1).functor;
          in
          if (f'.name == name && msome != null && mfail != null)
          then functor.type msome mfail
          else null;
        inherit functor;
        nestedTypes = { inherit some fail; };
      };
  };
}
