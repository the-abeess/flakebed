{libs, ...}:
let
  inherit (libs)
    any attrNames concatMap concatStringsSep count elem elemAt filter genList
    isAttrs length stripSpecialAttrs;
in
{
  flake.lib = {
    collectEvalFailures = skippedPathTemplates: set:
      let
        # NOTE: This likely has false positive, but at least it's not absurdly expensive ?
        preventCycle = path: any (e: count (e': e' == e) path > 2) path;

        f = path: set:
          let
            skippedPaths =
              map
              (template:
                genList
                (i:
                  let
                    offset = (length path) - (length template);
                    t =
                      if i - offset < 0 then "$"
                      else elemAt template (i - offset);
                  in
                  if t == "$" then elemAt path i
                  else t
                )
                (length path)
              )
              (filter (t: length t <= length path) skippedPathTemplates);
            names = attrNames (stripSpecialAttrs set);
            prettyPath =
              if path == [] then ""
             else " under ${concatStringsSep "." path}";
            recurse = n:
              let
                path' = path ++ [ n ];
              in
              if elem [ n ] skippedPathTemplates then [ ]
              else if
                ! (builtins.tryEval (builtins.hasAttr n set)).success
              then
                [{ path = path'; }]
              else f path' (builtins.getAttr n set);
            collected = 
              if preventCycle path then [ ]
              else if elem path skippedPaths then [ ]
              else if ! (builtins.tryEval set).success then
                [{ inherit path; }]
              else if ! (builtins.tryEval (builtins.seq set null)).success then
                [{ inherit path; }]
              else if ! isAttrs set then [ ]
              else if
                ! (builtins.tryEval (builtins.deepSeq names names)).success
              then
                [{ inherit path; }]
              else concatMap recurse names;
          in
            builtins.addErrorContext
            "while collecting evaluation failures${prettyPath}"
            collected;
      in
    f [ ] set;
  };
}
