# Generated from
# github.com/helix-editor/helix/blob/master/runtime/themes/fleet_dark.toml

# A take on the JetBrains Fleet theme. Feel free to contribute
# Original author: @krfl
# Contributors:
# @matoous
{
  palette = {
    background = "#181818";
    blue = "#52a7f6";
    blue_accent = "#2197F3";
    cyan = "#78d0bd";
    dark = "#898989";
    dark_gray = "#535353";
    darker = "#292929";
    darkest = "#1e1e1e";
    diff_plus = "#5A9F81";
    focus = "#204474";
    green = "#afcb85";
    green_accent = "#00AF99";
    light = "#d6d6dd";
    light_blue = "#7dbeff";
    light_gray = "#6d6d6d";
    lightest = "#ffffff";
    orange = "#efb080";
    orange_accent = "#EE7F25";
    pink = "#d898d8";
    pink_accent = "#E44C7A";
    purple = "#a390f0";
    red = "#CC7C8A";
    red_accent = "#F44747";
    red_error = "#EB5F6A";
    selection = "#1F3661";
    yellow = "#e5c995";
    yellow_accent = "#DEA407";
  };
  attribute = "green";
  comment = "light_gray";
  constant = "cyan";
  "constant.builtin.boolean" = "yellow";
  "constant.characted.escape" = "light";
  "constant.character" = "yellow";
  "constant.numeric" = "yellow";
  constructor = "yellow";
  diagnostic.modifiers = [ ];
  "diagnostic.error".underline = {
    color = "red_error";
    style = "line";
  };
  "diagnostic.hint".underline = {
    color = "light";
    style = "line";
  };
  "diagnostic.info".underline = {
    color = "blue_accent";
    style = "line";
  };
  "diagnostic.warning".underline = {
    color = "yellow_accent";
    style = "line";
  };
  "diff.delta" = "blue_accent";
  "diff.minus" = "red_accent";
  "diff.plus" = "diff_plus";
  error = "red_error";
  function = "yellow";
  "function.builtin" = "green";
  "function.macro" = "green";
  "function.method" = "light";
  "function.special" = "green";
  hint = "blue";
  info = "yellow_accent";
  keyword = "cyan";
  "keyword.control.exception" = "purple";
  label = "yellow";
  "markup.bold" = {
    fg = "lightest";
    modifiers = [ "bold" ];
  };
  "markup.heading" = {
    fg = "cyan";
    modifiers = [ "bold" ];
  };
  "markup.italic".modifiers = [ "italic" ];
  "markup.link.label" = "purple";
  "markup.link.text" = "cyan";
  "markup.link.url" = {
    fg = "pink";
    modifiers = [ "italic" "underlined" ];
  };
  "markup.list" = "pink";
  "markup.list.numbered" = "cyan";
  "markup.list.unnumbered" = "cyan";
  "markup.quote" = "pink";
  "markup.raw" = "pink";
  "markup.raw.block" = "pink";
  "markup.raw.inline" = "cyan";
  namespace = "light";
  operator = "light";
  punctuation = "light";
  special = "green";
  string = "pink";
  "string.regexp" = "light";
  "string.special" = {
    fg = "yellow";
    modifiers = [ "underlined" ];
  };
  tag = "light_blue";
  type = "light_blue";
  "type.enum.variant" = "purple";
  "ui.background" = {
    bg = "background";
  };
  "ui.cursor" = {
    bg = "dark_gray";
    modifiers = [ "reversed" ];
  };
  "ui.cursor.match" = {
    bg = "selection";
    fg = "light";
  };
  "ui.cursorline" = {
    bg = "darker";
  };
  "ui.help" = {
    bg = "darkest";
    fg = "light";
  };
  "ui.linenr" = "dark_gray";
  "ui.linenr.selected" = {
    bg = "darker";
    fg = "light";
  };
  "ui.menu" = {
    bg = "darkest";
    fg = "light";
  };
  "ui.menu.selected" = {
    bg = "focus";
    fg = "lightest";
  };
  "ui.popup" = {
    bg = "darkest";
    fg = "light";
  };
  "ui.selection" = {
    bg = "darker";
  };
  "ui.selection.primary" = {
    bg = "selection";
  };
  "ui.statusline" = {
    bg = "darker";
    fg = "light";
  };
  "ui.statusline.inactive" = {
    bg = "darker";
    fg = "dark";
  };
  "ui.statusline.insert" = {
    bg = "blue_accent";
    fg = "lightest";
  };
  "ui.statusline.normal" = {
    bg = "darker";
    fg = "lightest";
  };
  "ui.statusline.select" = {
    bg = "orange_accent";
    fg = "lightest";
  };
  "ui.text" = "light";
  "ui.text.focus" = {
    bg = "focus";
    fg = "lightest";
  };
  "ui.virtual" = "dark";
  "ui.virtual.ruler" = {
    bg = "darker";
  };
  "ui.window" = {
    bg = "darkest";
    fg = "dark";
  };
  variable = "light";
  "variable.builtin" = {
    fg = "red";
    modifiers = [ "underlined" ];
  };
  "variable.other.member" = "purple";
  "variable.parameter" = "light";
  warning = "orange_accent";
}
