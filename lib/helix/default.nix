{libs, ...}:
let
  inherit (libs) importTree removePrefix removeSuffix;
in
{
  flake.lib.helix = {
    themes =
      importTree
      ./themes 
      (path:
        {
          name =
          # A context in an attribute name doesnt't seem to be allowed
          # If we're wrong, it still doesn't make sense to attach context to
          # `name`, and `value` will still have it.
            builtins.unsafeDiscardStringContext
            (removeSuffix ".nix" (removePrefix "${./themes}/" path));
          value = import path;
        }
      )
      { "." = [
        "fleet_dark.nix"
      ]; };
  };
}
