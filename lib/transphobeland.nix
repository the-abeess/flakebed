{libs, ...}:
let
  inherit (libs) mkEnableOption mkOption toList types;
in
{
  flake.lib.types.transphobeland = {
    bool =
      types.coercedTo types.bool (b: if b then "true" else "false") types.str
      // { inherit (types.bool) name description; };
    modkey = types.enum
      [ "SHIFT" "ALT" "CTRL" "SUPER" "CAPS" "MOD2" "MOD3" "MOD5" ];

    mod =
      types.coercedTo
      types.transphobeland.modkey
      toList
      (types.noDupListOf types.transphobeland.modkey);

    # TODO: use a better typecheck than this
    key = types.either types.str types.int;

    # TODO: use enum
    dispatcher = types.str;

    monitorOffset = types.point2dOf types.int // {
      description = "position offset (in pixels) of a monitor";
    };

    bindCommand = types.submodule {
      options = {
        mod = mkOption {
          type = types.transphobeland.mod;
          example = "SUPER";
          description = "The optional bind modifier(s).";
        };

        key = mkOption {
          #TODO: use enum, document mouse
          type = types.transphobeland.key;
          example = "F";
          description = "The key or keycode to bind.";
        };

        cmd = mkOption {
          type = types.transphobeland.dispatcher;
          example = "exec";
          description = "The bind command dispatcher";
        };

        params = mkOption {
          type =
            types.coercedTo
            types.str
            toList
            (types.listOf types.str);
          default = "";
          description = ''
            The bind dispatcher's optional paramaters.
            See <a href="https://wiki.hyprland.org/Configuring/Dispatchers/">hyprland's wiki</a>.
          '';
        };

        whileLocked = mkEnableOption "whether this bind works even when inputs are inhibited.";

        onRelease = mkEnableOption "whether this bind triggers on release of a key.";

        repeat = mkEnableOption "whether this bind repeats when held.";

        mouse = mkEnableOption "whether this bind is a mouse bind.";
      };
    };

    monitorCommand = types.submoduleWith {
      modules = types.monitor.display.getSubModules;
    };
  };
}
