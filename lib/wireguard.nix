{libs, ...}:
let
  inherit (libs) wireguard;
in
{
  flake.lib.wireguard = {
    mkGenPreshared = { config, pkgs, wgNetwork, ... }: name:
      let
        wgExe = "${pkgs.wireguard-tools}/bin/wg";
        keyFile =
          let
            key = {
              kind = "preshared";
              network = wgNetwork.name;
              base = wgNetwork.keys.baseName;
              peer = name;
              inherit (wgNetwork.keys) extension;
            };
          in
          "${wgNetwork.keys.directory}/${wireguard.mkKeyName key}";
      in
      ''
        ${wgExe} genpsk > "${keyFile}"
      '';

    mkKeyName =
      { kind
      , network
      , base ? "wg"
      , peer ? null
      , extension ? "key"
      }: "${base}-${kind}-${network}"
        + (if isNull peer then "" else "-${peer}")
        + ".${extension}";
  };
}
