{libs, ...}:
let
  inherit (libs.nixpkgs)
    count defaultFunctor head isAttrs getFiles length mkOptionType showFiles
    showOption;
  inherit (libs.nixpkgs.types) optionDescriptionPhrase;
  inherit (libs.abeess) maybe;
in
{
  flake.lib = {
    maybe = {
      some = some: { inherit some; };
      none = { };

      isSome = mb: mb ? some;
      isNone = mb: ! maybe.isSome mb;

      unwrap = m: m.some or (throw "maybe: expected some value");
    };

      # A type which is an attribut set that may hold some value.
    types.maybe = wrapped:
      let
        name = "maybe";
      in
      mkOptionType {
        description =
          "maybe a(n) ${optionDescriptionPhrase (c: c == "noun") wrapped}";
        descriptionClass = "composite";
        check = x:
          isAttrs x
          && (x ? some -> wrapped.check x.some);
        merge = loc: defs:
          let nrNone = count (def: maybe.isNone def.value) defs;
          in if defs == [ ] then throw "unreachable"
          else if nrNone == length defs then (head defs).value
          else if nrNone == 0 then wrapped.merge loc defs
          else
            throw (
              "The option `${showOption loc}` is defined both as none and some, in "
                "${showFiles (getFiles defs)}"
            );
        emptyValue = { value = { }; };
        inherit (wrapped) getSubOptions getSubModules;
        substSubModules = m: libs.abeess.types.maybe (wrapped.subsSubModules m);
        functor = (defaultFunctor name) // { inherit wrapped; };
        nestedTypes = { inherit wrapped; };
      };
  };
}
