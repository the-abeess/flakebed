{
  description = "A lightweight collection of nix flake utilities";

  inputs = {
    parts.url = "github:hercules-ci/flake-parts";
  };

  outputs = inputs:
    inputs.parts.lib.mkFlake
    { inherit inputs; }
    {
      imports = [ ./flake ];
      systems = inputs.parts.inputs.nixpkgs-lib.lib.systems.flakeExposed;
      # debug = true;
    };
}
