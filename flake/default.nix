{
  imports = [
    ../lib
    ./checks.nix
    ./flake-modules.nix
    ./templates.nix
  ];

  crossPackages.enable = true;
}
