{ config, inputs, self, ... }:
let
  args = {
    inherit config inputs self;
  };

  importApply = path: arg: {
    _file = path;
    imports = [ (import path arg) ];
  };

  flakeModules = {
    lib = importApply ../parts/libs.nix args;
    nixpkgs = importApply ../parts/nixpkgs.nix args;
    crossPackages = importApply ../parts/cross-packages.nix args;
    systems  = importApply ../parts/systems.nix args;
  };

  allFlakeModules = builtins.attrValues flakeModules;
in
{
  imports = allFlakeModules;

  flake.flakeModules = flakeModules // { default.imports = allFlakeModules; };
}
