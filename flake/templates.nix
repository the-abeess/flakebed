{ self, ... }:
let
  templates.rust = templates.rust-nci;

  templates = {
    rust-d2n = {
      path = "${self}/templates/rust-d2n";
      description = "A simple Rust/Cargo project based on dream2nix";
    };
    rust-nci = {
      path = "${self}/templates/rust-nci";
      description = "A simple Rust/Cargo project based on nix-cargo-integration";
    };
    rust-nci-bevy = {
      path = "${self}/templates/rust-nci-bevy";
      description = "A simple Bevy plugin/app based on nix-cargo-integration";
    };
  };
in {
  flake = { inherit templates; };
}
