{ config, libs, ... }:
let
  topCfg = config;

  collectEvalFailures = libs.collectEvalFailures [
    [ "debug" "$" ]
    [ "checks" "$" ]
    [ "allModuleArgs" ]
    [ "allSystems" "$" ]
  ];

  checkStrEqualScript = builtins.toFile "check-str-equal.sh" ''
    set -euo pipefail
    echo "expected: $1"
    echo "actual: $2"
    [ "$1" = "$2" ] && echo "success" > $out
  '';
in {
  perSystem = { config, system, ... }:
    let
      mkEvalCheck = name: expected: evalSet: builtins.derivation {
        inherit name system;
        src = ./.;
        builder = "/bin/sh";
        args = [
          checkStrEqualScript
          "${builtins.toJSON expected}"
          "${builtins.toJSON (collectEvalFailures evalSet)}"
        ];
      };
    in
    {
    checks.perSystemConfigEvaluation =
      mkEvalCheck
      "abeess-check-perSystemConfigEvaluation"
      [
        { path = [ "nixpkgs" "pkgs" ]; }
        { path = [ "nixpkgs" "source" ]; }
      ]
      config;
    checks.flakeConfigEvaluation =
      mkEvalCheck
      "abeess-check-flakeConfigEvaluation"
      [ { path = [ "nixpkgs" "source" ]; } ]
      topCfg;
  };
}
